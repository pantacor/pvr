//
// Copyright 2017-2023  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package main

import (
	"crypto/tls"
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"os/user"
	"path/filepath"
	"syscall"

	"github.com/bgentry/go-netrc/netrc"
	"github.com/go-resty/resty"
	"github.com/urfave/cli"
	"gitlab.com/pantacor/pvr/cmd"
	"gitlab.com/pantacor/pvr/cmd/app"
	"gitlab.com/pantacor/pvr/cmd/device"
	"gitlab.com/pantacor/pvr/cmd/dm"
	"gitlab.com/pantacor/pvr/cmd/fakeroot"
	"gitlab.com/pantacor/pvr/cmd/sig"
	"gitlab.com/pantacor/pvr/libpvr"
)

// ReadUserPasswdFromNetrc reads username and password from the .netrc file
// located in the current directory or $HOME/.netrc
func ReadUserPasswdFromNetrc(host string) (string, string, error) {
	path := ".netrc"
	_, err := os.Stat(path)
	if err != nil {
		homeDir, err := os.UserHomeDir()
		if err != nil {
			return "", "", err
		}
		path = filepath.Join(homeDir, ".netrc")
		_, err = os.Stat(path)
		if err != nil {
			return "", "", err
		}
	}

	f, err := os.Open(path)
	if err != nil {
		return "", "", err
	}
	defer f.Close()

	file, err := netrc.Parse(f)
	if err != nil {
		return "", "", err
	}

	machine := file.FindMachine(host)
	if machine == nil {
		return "", "", errors.New("no matching machine found in .netrc")
	}

	return machine.Login, machine.Password, nil
}

func main() {
	cliApp := cli.NewApp()
	cliApp.EnableBashCompletion = true
	cliApp.Name = "pvr"
	cliApp.Usage = "PantaVisor Repo"
	cliApp.Version = VERSION
	home := os.Getenv("HOME")

	if home == "" {
		usr, err := user.Current()
		if err != nil {
			euid := syscall.Geteuid()
			usr, err = user.LookupId(fmt.Sprintf("%d", euid))
		}
		if err != nil {
			panic(err)
		}
		home = usr.HomeDir
	}
	cliApp.Flags = []cli.Flag{
		cli.StringFlag{
			Name:   "user, u",
			Usage:  "Username used for authentication with core services",
			EnvVar: "PVR_USERNAME",
		},
		cli.StringFlag{
			Name:   "password, p",
			Usage:  "Password used for authentication with core services",
			EnvVar: "PVR_PASSWORD",
		},
		cli.StringFlag{
			Name:   "access-token, a",
			Usage:  "Use `ACCESS_TOKEN` for authorization with core services",
			EnvVar: "PVR_ACCESSTOKEN",
		},
		cli.StringFlag{
			Name:   "baseurl, b",
			Usage:  "Use `BASEURL` for resolving prn URIs to core service endpoints",
			EnvVar: "PVR_BASEURL",
			Value:  "https://api.pantahub.com",
		},
		cli.StringFlag{
			Name:   "http-proxy",
			Usage:  "Use `PVR_HTTP_PROXY` to configure the proxy mode. Values 'no' (disable), '<URL>' (use proxy by url), 'system' (use system settings - default)",
			EnvVar: "PVR_HTTP_PROXY",
			Value:  "system",
		},
		cli.StringFlag{
			Name:   "repo-baseurl, r",
			Usage:  "Use `PVR_REPO_BASEURL` for resolving PVR repositories like docker through user/name syntax.",
			EnvVar: "PVR_REPO_BASEURL",
			Value:  "https://pvr.pantahub.com",
		},
		cli.StringFlag{
			Name:   "config-dir, c",
			Usage:  "Use `PVR_CONFIG_DIR` for using a custom global config directory (used to store auth.json etc.).",
			EnvVar: "PVR_CONFIG_DIR",
			Value:  filepath.Join(home, ".pvr"),
		},
		cli.BoolFlag{
			Name:   "debug, d",
			Usage:  "enable debugging output for rest calls",
			EnvVar: "PVR_DEBUG",
		},
		cli.BoolFlag{
			Name:   "disable-self-upgrade",
			Usage:  "disable self-upgrades",
			EnvVar: "PVR_DISABLE_SELF_UPGRADE",
		},
		cli.BoolFlag{
			Name:   "insecure, i",
			Usage:  "skip tls verify",
			EnvVar: "PVR_INSECURE",
		},
	}

	cliApp.Before = func(c *cli.Context) error {
		libpvr.IsDebugEnabled = c.GlobalBool("debug")
		resty.SetTLSClientConfig(&tls.Config{InsecureSkipVerify: c.GlobalBool("insecure")})
		resty.SetDebug(libpvr.IsDebugEnabled)

		c.App.Metadata["PVR_AUTH"] = c.GlobalString("access-token")

		if c.GlobalString("baseurl") != "" {
			c.App.Metadata["PVR_BASEURL"] = c.GlobalString("baseurl")
		} else {
			c.App.Metadata["PVR_BASEURL"] = "https://api.pantahub.com"
		}

		baseURL, err := url.Parse(c.App.Metadata["PVR_BASEURL"].(string))
		if err != nil {
			return err
		}
		c.App.Metadata["PVR_BASEURL_url"] = baseURL

		if c.GlobalString("repo-baseurl") != "" {
			c.App.Metadata["PVR_REPO_BASEURL"] = c.GlobalString("repo-baseurl")
		} else {
			c.App.Metadata["PVR_REPO_BASEURL"] = "https://pvr.pantahub.com"
		}

		repoBaseURL, err := url.Parse(c.App.Metadata["PVR_REPO_BASEURL"].(string))
		if err != nil {
			return err
		}
		c.App.Metadata["PVR_REPO_BASEURL_url"] = repoBaseURL

		if c.GlobalString("config-dir") != "" {
			c.App.Metadata["PVR_CONFIG_DIR"] = c.GlobalString("config-dir")
		} else {
			c.App.Metadata["PVR_CONFIG_DIR"] = filepath.Join(home, ".pvr")
		}

		if !c.GlobalBool("disable-self-upgrade") {
			c.App.Metadata["PVR_SELF_UPGRADE"] = "yes"
		} else {
			c.App.Metadata["PVR_SELF_UPGRADE"] = nil
		}

		transport := http.DefaultTransport.(*http.Transport)

		// --http-proxy=no -> disable proxy even if http_proxy env is set
		if c.GlobalString("http-proxy") == "system" {
			transport.Proxy = http.ProxyFromEnvironment
		} else if c.GlobalString("http-proxy") == "no" {
			transport.Proxy = nil
		} else {
			u, err := url.Parse(c.GlobalString("http-proxy"))
			if err != nil {
				return errors.New("ERROR: http-proxy not a valid url")
			}
			transport.Proxy = http.ProxyURL(u)
		}

		user, pass, _ := ReadUserPasswdFromNetrc(baseURL.Host) // ignore error
		if user != "" && pass != "" {
			c.App.Metadata["PVR_USERNAME"] = user
			c.App.Metadata["PVR_PASSWORD"] = pass
		}

		if c.GlobalString("user") != "" && c.GlobalString("password") != "" {
			c.App.Metadata["PVR_USERNAME"] = c.GlobalString("user")
			c.App.Metadata["PVR_PASSWORD"] = c.GlobalString("password")
		}

		resty.SetTransport(transport)

		libpvr.UpdateIfNecessary(c)

		return nil
	}

	cliApp.Commands = []cli.Command{
		cmd.CommandInit(),
		cmd.CommandAdd(),
		cmd.CommandJson(),
		cmd.CommandClaim(),
		cmd.CommandDiff(),
		cmd.CommandStatus(),
		cmd.CommandCommit(),
		cmd.CommandPut(),
		cmd.CommandPost(),
		cmd.CommandGet(),
		cmd.CommandInspect(),
		cmd.CommandRemoteInfo(),
		cmd.CommandStepInfo(),
		cmd.CommandDeploy(),
		cmd.CommandMerge(),
		cmd.CommandReset(),
		cmd.CommandClone(),
		cmd.CommandFastCopy(),
		cmd.CommandPutObjects(),
		cmd.CommandExport(),
		cmd.CommandImport(),
		cmd.CommandRegister(),
		cmd.CommandScanDeprecated(),
		cmd.CommandPsDeprecated(),
		cmd.CommandLogsDeprecated(),
		cmd.CommandSelfUpgrade(),
		cmd.CommandGlobalConfig(),
		cmd.CommandWhoami(),
		cmd.CommandLogin(),
		cmd.CommandCompletion(),
		cmd.CommandVersion(),
		// SubCommands
		dm.CommandDmApply(),
		dm.CommandDmConvert(),
		device.CommandDevice(),
		app.CommandApp(),
		sig.CommandSig(),
		fakeroot.CommandFakeroot(),
	}

	err := cliApp.Run(os.Args)
	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %s\n", err.Error())
		os.Exit(1)
	}
}
